# Translation of libkirigami2plugin_qt.po to Brazilian Portuguese
# Copyright (C) 2016-2019 This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Luiz Fernando Ranghetti <elchevive@opensuse.org>, 2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023.
# André Marcelo Alvarenga <alvarenga@kde.org>, 2018, 2019.
# Thiago Masato Costa Sueto <herzenschein@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: libkirigami2plugin_qt\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"PO-Revision-Date: 2023-01-16 23:04-0300\n"
"Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>\n"
"Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Qt-Contexts: true\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 21.12.3\n"

#: controls/AboutItem.qml:139
#, qt-format
msgctxt "AboutItem|"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: controls/AboutItem.qml:148
#, qt-format
msgctxt "AboutItem|"
msgid "Send an email to %1"
msgstr "Enviar e-mail para %1"

#: controls/AboutItem.qml:198
msgctxt "AboutItem|"
msgid "Get Involved"
msgstr "Participe"

#: controls/AboutItem.qml:204
msgctxt "AboutItem|"
msgid "Donate"
msgstr "Doar"

#: controls/AboutItem.qml:221
msgctxt "AboutItem|"
msgid "Report a Bug"
msgstr "Relatar erro"

#: controls/AboutItem.qml:234
msgctxt "AboutItem|"
msgid "Copyright"
msgstr "Copyright"

#: controls/AboutItem.qml:278
msgctxt "AboutItem|"
msgid "License:"
msgstr "Licença:"

#: controls/AboutItem.qml:300
#, qt-format
msgctxt "AboutItem|"
msgid "License: %1"
msgstr "Licença: %1"

#: controls/AboutItem.qml:311
msgctxt "AboutItem|"
msgid "Libraries in use"
msgstr "Bibliotecas em uso"

#: controls/AboutItem.qml:341
msgctxt "AboutItem|"
msgid "Authors"
msgstr "Autores"

#: controls/AboutItem.qml:350
msgctxt "AboutItem|"
msgid "Show author photos"
msgstr "Mostrar as fotos dos autores"

#: controls/AboutItem.qml:381
msgctxt "AboutItem|"
msgid "Credits"
msgstr "Créditos"

#: controls/AboutItem.qml:394
msgctxt "AboutItem|"
msgid "Translators"
msgstr "Tradutores"

#: controls/AboutPage.qml:93
#, qt-format
msgctxt "AboutPage|"
msgid "About %1"
msgstr "Sobre %1"

#: controls/AbstractApplicationWindow.qml:204
msgctxt "AbstractApplicationWindow|"
msgid "Quit"
msgstr "Sair"

#: controls/ActionToolBar.qml:205
msgctxt "ActionToolBar|"
msgid "More Actions"
msgstr "Mais ações"

#: controls/Avatar.qml:182
#, qt-format
msgctxt "Avatar|"
msgid "%1 — %2"
msgstr "%1 — %2"

#: controls/Chip.qml:82
msgctxt "Chip|"
msgid "Remove Tag"
msgstr "Remover etiqueta"

#: controls/ContextDrawer.qml:66
msgctxt "ContextDrawer|"
msgid "Actions"
msgstr "Ações"

#: controls/GlobalDrawer.qml:506
msgctxt "GlobalDrawer|"
msgid "Back"
msgstr "Voltar"

#: controls/GlobalDrawer.qml:599
msgctxt "GlobalDrawer|"
msgid "Close Sidebar"
msgstr "Fechar barra lateral"

#: controls/GlobalDrawer.qml:602
msgctxt "GlobalDrawer|"
msgid "Open Sidebar"
msgstr "Abrir barra lateral"

#: controls/LoadingPlaceholder.qml:54
msgctxt "LoadingPlaceholder|"
msgid "Loading…"
msgstr "Carregando..."

#: controls/PasswordField.qml:42
msgctxt "PasswordField|"
msgid "Password"
msgstr "Senha"

#: controls/private/globaltoolbar/PageRowGlobalToolBarUI.qml:70
msgctxt "PageRowGlobalToolBarUI|"
msgid "Close menu"
msgstr "Fechar menu"

#: controls/private/globaltoolbar/PageRowGlobalToolBarUI.qml:70
msgctxt "PageRowGlobalToolBarUI|"
msgid "Open menu"
msgstr "Abrir menu"

#: controls/SearchField.qml:94
msgctxt "SearchField|"
msgid "Search…"
msgstr "Pesquisar..."

#: controls/SearchField.qml:96
msgctxt "SearchField|"
msgid "Search"
msgstr "Pesquisar"

#: controls/SearchField.qml:107
msgctxt "SearchField|"
msgid "Clear search"
msgstr "Limpar pesquisa"

#: controls/settingscomponents/CategorizedSettings.qml:40
#: controls/settingscomponents/CategorizedSettings.qml:104
msgctxt "CategorizedSettings|"
msgid "Settings"
msgstr "Configurações"

#: controls/settingscomponents/CategorizedSettings.qml:40
#, qt-format
msgctxt "CategorizedSettings|"
msgid "Settings — %1"
msgstr "Configurações — %1"

#. Accessibility text for a page tab. Keep the text as concise as possible and don't use a percent sign.
#: controls/swipenavigator/templates/PageTab.qml:38
#, qt-format
msgctxt "PageTab|"
msgid "Current page. Progress: %1 percent."
msgstr "Página atual. Progresso: %1 porcento."

#. Accessibility text for a page tab. Keep the text as concise as possible.
#: controls/swipenavigator/templates/PageTab.qml:41
#, qt-format
msgctxt "PageTab|"
msgid "Navigate to %1. Progress: %2 percent."
msgstr "Navegar para %1. Progresso: %2 porcento."

#. Accessibility text for a page tab. Keep the text as concise as possible.
#: controls/swipenavigator/templates/PageTab.qml:46
msgctxt "PageTab|"
msgid "Current page."
msgstr "Página atual."

#. Accessibility text for a page tab that's requesting the user's attention. Keep the text as concise as possible.
#: controls/swipenavigator/templates/PageTab.qml:49
#, qt-format
msgctxt "PageTab|"
msgid "Navigate to %1. Demanding attention."
msgstr "Navegar para %1. Demandando atenção."

#. Accessibility text for a page tab that's requesting the user's attention. Keep the text as concise as possible.
#: controls/swipenavigator/templates/PageTab.qml:52
#, qt-format
msgctxt "PageTab|"
msgid "Navigate to %1."
msgstr "Navegar para %1."

#: controls/templates/OverlayDrawer.qml:130
#, fuzzy
#| msgctxt "GlobalDrawer|"
#| msgid "Close Sidebar"
msgctxt "OverlayDrawer|"
msgid "Close drawer"
msgstr "Fechar barra lateral"

#: controls/templates/OverlayDrawer.qml:136
msgctxt "OverlayDrawer|"
msgid "Open drawer"
msgstr ""

#: controls/templates/private/BackButton.qml:34
msgctxt "BackButton|"
msgid "Navigate Back"
msgstr "Voltar"

#: controls/templates/private/ForwardButton.qml:32
msgctxt "ForwardButton|"
msgid "Navigate Forward"
msgstr "Avançar"

#: controls/ToolBarApplicationHeader.qml:89
msgctxt "ToolBarApplicationHeader|"
msgid "More Actions"
msgstr "Mais ações"

#: controls/UrlButton.qml:51
msgctxt "UrlButton|"
msgid "Copy Link to Clipboard"
msgstr "Copiar link para a área de transferência"

#: settings.cpp:211
#, qt-format
msgctxt "Settings|"
msgid "KDE Frameworks %1"
msgstr "KDE Frameworks %1"

#: settings.cpp:213
#, qt-format
msgctxt "Settings|"
msgid "The %1 windowing system"
msgstr "O sistema de janelas %1"

#: settings.cpp:214
#, qt-format
msgctxt "Settings|"
msgid "Qt %2 (built against %3)"
msgstr "Qt %2 (compilado com %3)"

#~ msgctxt "AboutItem|"
#~ msgid "Visit %1's KDE Store page"
#~ msgstr "Visite a página do %1 na KDE Store"

#~ msgctxt "UrlButton|"
#~ msgid "Copy link address"
#~ msgstr "Copiar endereço do link"

#~ msgctxt "AboutItem|"
#~ msgid "(%1)"
#~ msgstr "(%1)"

#~ msgctxt "SearchField|"
#~ msgid "Search..."
#~ msgstr "Pesquisar..."

#~ msgctxt "AboutPage|"
#~ msgid "%1 <%2>"
#~ msgstr "%1 <%2>"
